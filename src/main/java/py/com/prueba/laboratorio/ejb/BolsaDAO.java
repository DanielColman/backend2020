package py.com.prueba.laboratorio.ejb;

import py.com.prueba.laboratorio.modelo.Bolsa;
import py.com.prueba.laboratorio.modelo.Persona;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@Stateless
public class BolsaDAO {

    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(Bolsa entidad) {


        //Fecha Asignado
        entidad.setFechaAsignado(LocalDate.now());

        //Fecha de Vencimiento
        Query q0 = this.em.createQuery( "select v.fechaFin from Vencimiento v Where v.idVencimiento = :id" );
        q0.setParameter("id", entidad.getIdVencimiento());
        entidad.setFechaVencimiento( (LocalDate) q0.getSingleResult() );

        //Asignacion de Punto
        Query q = this.em.createQuery( "select r.monto from Regla r Where r.idRegla = :id" );
        q.setParameter("id", entidad.getIdRegla());
        entidad.setPuntaje((int) Math.ceil((entidad.getMonto()-1) / (int) q.getSingleResult() ) + 1);

        //Puntaje Utilizado se Establece en 0
        entidad.setPuntajeUtilizado(0);

        //Calculo de Saldo
        entidad.setSaldo(entidad.getPuntaje() - entidad.getPuntajeUtilizado());

        //Estado
        entidad.setEstado("A");



        this.em.persist(entidad);

        /*Query q = this.em.createQuery( "Update Bolsa b set b.puntaje = :puntaje Where b.idBolsa = :id" );
        q.setParameter("puntaje", 100);
        q.setParameter("id", entidad.getIdBolsa());
        q.executeUpdate();*/

    }

    public void modificar(Bolsa entidad) {

        //Fecha Asignado
        Query q = this.em.createQuery( "select b.fechaAsignado from Bolsa b Where b.idBolsa = :id" );
        q.setParameter("id", entidad.getIdBolsa());
        entidad.setFechaAsignado((LocalDate) q.getSingleResult());

        //Asignacion de Monto
        Query q2 = this.em.createQuery( "select r.monto from Regla r Where r.idRegla = :id" );
        q2.setParameter("id", entidad.getIdRegla());
        entidad.setPuntaje((int) Math.ceil((entidad.getMonto()-1) / (int) q2.getSingleResult() ) + 1);

        //Puntaje Utilizado se Mantiene
        Query q3 = this.em.createQuery( "select b.puntajeUtilizado from Bolsa b Where b.idBolsa = :id" );
        q3.setParameter("id", entidad.getIdBolsa());
        entidad.setPuntajeUtilizado((int) q3.getSingleResult());

        //Fecha de Vencimiento
        Query q4 = this.em.createQuery( "select v.fechaFin from Vencimiento v Where v.idVencimiento = :id" );
        q4.setParameter("id", entidad.getIdVencimiento());
        entidad.setFechaVencimiento( (LocalDate) q4.getSingleResult() );

        //Calculo de Saldo
        entidad.setSaldo(entidad.getPuntaje() - entidad.getPuntajeUtilizado());

        //Estado
        entidad.setEstado("A");


        this.em.merge(entidad);
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(Bolsa.class,id));
    }

    @SuppressWarnings("unchecked")
    public List<Bolsa> lista() {
        Query q = em.createQuery("SELECT b FROM Bolsa b");
        return (List<Bolsa>)q.getResultList();
    }

    public List<Bolsa> lista(String id){

        Query q = this.em.createQuery( "select p.idPersona from Persona p Where p.nroDocumento = :id" );
        q.setParameter("id", id);

        Query q1 = this.em.createQuery( "SELECT p FROM Bolsa p Where p.idCliente = :id" );
        q1.setParameter("id", q.getSingleResult());


        return (List<Bolsa>)q1.getResultList();
    }

    public List<Bolsa> listaRango(Integer rango1, Integer rango2) {
        Query q = em.createQuery("SELECT b FROM Bolsa b where  b.saldo >= :rango1 and b.saldo <= :rango2");
        q.setParameter("rango1", rango1).setParameter("rango2", rango2);
        return (List<Bolsa>)q.getResultList();
    }

    public List<Persona> listaVence(Integer dias) {

        LocalDate fechaActual = LocalDate.now();
        LocalDate fechaEnDias = fechaActual.plusDays(dias);

        Query q = this.em.createQuery( "SELECT p.nombre,p.apellido,b FROM Persona p, Bolsa b " +
                "Where b.fechaVencimiento <= :id and p.idPersona = b.idCliente and b.estado = 'A'" );

        q.setParameter("id", fechaEnDias);

        return (List<Persona>)q.getResultList();
    }


}
