package py.com.prueba.laboratorio.ejb;

import py.com.prueba.laboratorio.modelo.Persona;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Stateless
public class PersonaDAO {
    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(Persona entidad) {
        this.em.persist(entidad);
    }

    public void modificar(Persona entidad) {
        this.em.merge(entidad);
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(Persona.class,id));
    }

    @SuppressWarnings("unchecked") //Solo suprimimos el Warning
    public List<Persona> lista() {
        Query q = em.createQuery("SELECT p FROM Persona p");
        return (List<Persona>)q.getResultList();
    }

    @SuppressWarnings("unchecked") //Solo suprimimos el Warning
    public List<Persona> lista(String id){

        Query q = this.em.createQuery( "SELECT p.idPersona FROM Persona p Where p.nroDocumento= :id" );
        q.setParameter("id", id);

        Query q1 = this.em.createQuery( "SELECT p FROM Persona p Where p.idPersona = :id" );
        q1.setParameter("id", q.getSingleResult());

        return (List<Persona>)q1.getResultList();
    }

    @SuppressWarnings("unchecked") //Solo suprimimos el Warning
    public List<Persona> listaNombre(String nombre){

        Query q = this.em.createQuery( "SELECT p FROM Persona p Where  lower(p.nombre) like lower(:id) " );
        String par= "%";
        par = par.concat(nombre);
        par = par.concat("%");
        q.setParameter("id", par);

        return (List<Persona>)q.getResultList();
    }

    @SuppressWarnings("unchecked") //Solo suprimimos el Warning
    public List<Persona> listaApellido(String apellido){

        Query q = this.em.createQuery( "SELECT p FROM Persona p Where lower(p.apellido) like lower(:id)" );
        String par= "%";
        par = par.concat(apellido);
        par =par.concat("%");
        q.setParameter("id", par);

        return (List<Persona>)q.getResultList();
    }

    public List<Persona> listaCumple(String cumple){

        Query q = this.em.createQuery( "SELECT p FROM Persona p Where p.fechaNac = :id" );
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        formatter = formatter.withLocale( Locale.US );
        LocalDate date = LocalDate.parse(cumple, formatter);

        q.setParameter("id", date);

        return (List<Persona>)q.getResultList();
    }

}
