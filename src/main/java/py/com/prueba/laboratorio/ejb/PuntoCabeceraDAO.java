package py.com.prueba.laboratorio.ejb;

import py.com.prueba.laboratorio.modelo.Persona;
import py.com.prueba.laboratorio.modelo.PuntoCabecera;
import py.com.prueba.laboratorio.modelo.PuntoDetalle;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@Stateless
public class PuntoCabeceraDAO {
    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(PuntoCabecera entidad) {

        //Recupera el Concepto de Uso de Puntos
        Query q = this.em.createQuery( "select v.nombre from Vale v Where v.idVale = :id" );
        q.setParameter("id", entidad.getIdVale());
        entidad.setConceptoVale((String) q.getSingleResult());

        Query q1 = this.em.createQuery( "select v.puntosReq from Vale v Where v.idVale = :id" );
        q1.setParameter("id", entidad.getIdVale());
        entidad.setPuntaje_usado((int) q1.getSingleResult());

        entidad.setFecha_uso(LocalDate.now());

        int puntaje = entidad.getPuntaje_usado();

        this.em.persist(entidad);

        int puntajeAUX=puntaje;

        while (puntajeAUX != 0){

            Query q3 = this.em.createQuery( "select MIN (b.idBolsa) from Bolsa b where b.estado = 'A' " +
                    "and b.idCliente = :id");
            q3.setParameter("id",entidad.getIdPersona());

            Query q2 = this.em.createQuery( "select b.saldo " +
                            "from Bolsa b " +
                            "Where  (b.idCliente = :id) and (b.estado = 'A') and (b.idBolsa = :id2) "
            );
            q2.setParameter("id", entidad.getIdPersona()).setParameter("id2", q3.getSingleResult());

            int aux = (int) q2.getSingleResult();
            int vguarda, saldo;
            boolean banderaI;

            if(aux <= puntajeAUX) {
                puntajeAUX=puntajeAUX-(int) q2.getSingleResult();
                vguarda = (int) q2.getSingleResult();
                saldo = 0;
                banderaI = true;
            }
            else{
                saldo = (int) q2.getSingleResult() - puntajeAUX;
                vguarda=puntajeAUX;
                puntajeAUX = 0;
                banderaI = false;

            }

            //Se crea eL Objeto Punto Detalle
            PuntoDetalle pd = new PuntoDetalle();
            pd.setIdPuntoCabecera(entidad.getIdPuntoCabecera());
            pd.setIdBolsa((int)q3.getSingleResult());
            pd.setPuntaje_usado(vguarda);

            this.em.persist(pd);

            String estado;
            if (banderaI){
                estado = "I";
            }else {
                estado = "A";
            }


            Query q4 = this.em.createQuery( "update Bolsa b " +
                    "set b.estado = :estado, b.saldo = :saldo, " +
                    "b.puntajeUtilizado = b.puntajeUtilizado + :pUtilizado  " +
                    "where b.idBolsa = :id");
            q4.setParameter("id", q3.getSingleResult()).setParameter("saldo", saldo)
                    .setParameter("pUtilizado",vguarda)
                    .setParameter("estado",estado);
            q4.executeUpdate();


        }


    }

    public void modificar(PuntoCabecera  entidad) {
        this.em.merge(entidad);
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(PuntoCabecera .class,id));
    }


    @SuppressWarnings("unchecked")
    public List<PuntoCabecera> lista() {
        Query q = em.createQuery("SELECT p FROM PuntoCabecera p");
        return (List<PuntoCabecera>)q.getResultList();
    }

    public List<PuntoCabecera> listaCliente( String id) {

        Query q = this.em.createQuery( "SELECT p.idPersona FROM Persona p Where p.nroDocumento= :id" );
        q.setParameter("id", id);

        Query q2 = em.createQuery("SELECT pc FROM PuntoCabecera pc where pc.idPersona = :idPersona");
        q2.setParameter("idPersona", (int) q.getSingleResult());

        return (List<PuntoCabecera>)q2.getResultList();
    }

    public List<PuntoCabecera> listaFecha(String fecha){

        Query q = this.em.createQuery( "SELECT pc FROM PuntoCabecera pc Where pc.fecha_uso = :id  " );
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        formatter = formatter.withLocale( Locale.US );
        LocalDate date = LocalDate.parse(fecha, formatter);

        q.setParameter("id", date);

        return (List<PuntoCabecera>)q.getResultList();
    }

    public List<PuntoCabecera> listaUso( Integer id) {
        Query q = em.createQuery("SELECT p FROM PuntoCabecera p where p.idVale = :id");
        q.setParameter("id", id);
        return (List<PuntoCabecera>)q.getResultList();
    }


}