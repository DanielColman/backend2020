package py.com.prueba.laboratorio.ejb;

import py.com.prueba.laboratorio.modelo.PuntoDetalle;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.List;

@Stateless
public class PuntoDetalleDAO {
    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(PuntoDetalle entidad) {
        this.em.persist(entidad);
    }

    public void modificar(PuntoDetalle  entidad) {
        this.em.merge(entidad);
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(PuntoDetalle .class,id));
    }


    @SuppressWarnings("unchecked")
    public List<PuntoDetalle> lista() {
        Query q = em.createQuery("SELECT p FROM PuntoDetalle p");
        return (List<PuntoDetalle>)q.getResultList();
    }

}