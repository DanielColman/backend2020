package py.com.prueba.laboratorio.ejb;


import py.com.prueba.laboratorio.modelo.Persona;
import py.com.prueba.laboratorio.modelo.Regla;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class ReglaDAO {
    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(Regla entidad) {
        this.em.persist(entidad);
    }

    public void modificar(Regla entidad) {
        this.em.merge(entidad);
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(Regla.class,id));
    }

    @SuppressWarnings("unchecked")
    public List<Regla> lista() {
        Query q = em.createQuery("SELECT r FROM Regla r");
        return (List<Regla>)q.getResultList();
    }

    public List<Integer> cuantos(Integer idRegla, Integer monto) {
        Query q2 = this.em.createQuery( "select r.monto from Regla r Where r.idRegla = :id" );
        q2.setParameter("id", idRegla);
        Integer puntaje = (int) Math.ceil((monto-1) / (int) q2.getSingleResult() ) + 1;

        List<Integer> listPUntaje = new ArrayList<>();

        listPUntaje.add(puntaje);

        return (List<Integer>)listPUntaje;
    }
}
