package py.com.prueba.laboratorio.ejb;

import py.com.prueba.laboratorio.modelo.Vale;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ValeDAO {
    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(Vale entidad) {
        this.em.persist(entidad);
    }

    public void modificar(Vale entidad) {
        this.em.merge(entidad);
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(Vale.class,id));
    }

    @SuppressWarnings("unchecked")
    public List<Vale> lista() {
        Query q = em.createQuery("SELECT v FROM Vale v");
        return (List<Vale>)q.getResultList();
    }

}
