package py.com.prueba.laboratorio.ejb;

import py.com.prueba.laboratorio.modelo.Vencimiento;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Stateless
public class VencimientoDAO {

    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(Vencimiento vencimiento) {
        vencimiento.setDuracionDias(
                (int) ChronoUnit.DAYS.between(
                        vencimiento.getFechaInicio(), vencimiento.getFechaFin()
                )
        );

        this.em.persist(vencimiento);
    }

    public void modificar(Vencimiento vencimiento) {
        vencimiento.setDuracionDias(
                (int) ChronoUnit.DAYS.between(
                        vencimiento.getFechaInicio(), vencimiento.getFechaFin()
                )
        );
        this.em.merge(vencimiento);

    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(Vencimiento.class,id));
    }

    @SuppressWarnings("unchecked")
    public List<Vencimiento> lista() {
        Query q = em.createQuery("SELECT v FROM Vencimiento v");
        return (List<Vencimiento>)q.getResultList();
    }

}
