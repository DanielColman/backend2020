package py.com.prueba.laboratorio.modelo;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.persistence.*;
import javax.ws.rs.DefaultValue;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "bolsa")
public class Bolsa implements Serializable {


    @Id
    @Column(name = "id_bolsa", nullable = false)
    @Basic(optional = false)
    @GeneratedValue(generator = "bolsaSec")
    @SequenceGenerator(name = "bolsaSec",sequenceName = "bolsa_sec",allocationSize = 0)
    private Integer idBolsa;

    @Column(name = "id_cliente",nullable = false)
    private Integer idCliente;

    @Column(name = "fecha_asignado", length = 50,nullable = false)
    @JsonbDateFormat(value = "dd-MM-yyyy")
    private LocalDate fechaAsignado;

    @Column(name = "monto", length = 50,nullable = false)
    private Integer monto;

    @Column(name = "id_vencimiento", length = 50,nullable = false)
    private Integer idVencimiento;

    @Column(name = "fecha_vencimiento", length = 50,nullable = false)
    @JsonbDateFormat(value = "dd-MM-yyyy")
    private LocalDate fechaVencimiento;

    @Column(name = "id_regla", length = 50,nullable = false)
    private Integer idRegla;

    @Column(name = "puntaje", length = 50)
    private Integer puntaje;

    @Column(name = "puntaje_utilizado", length = 50)
    private Integer puntajeUtilizado;

    @Column(name = "saldo", length = 50)
    private Integer saldo;

    @Column(name = "estado", length = 1)
    private String estado ;

    public Integer getIdBolsa() {
        return idBolsa;
    }

    public void setIdBolsa(Integer idBolsa) {
        this.idBolsa = idBolsa;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public LocalDate getFechaAsignado() {
        return fechaAsignado;
    }

    public void setFechaAsignado(LocalDate fechaAsignado) {
        this.fechaAsignado = fechaAsignado;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Integer getIdVencimiento() {
        return idVencimiento;
    }

    public void setIdVencimiento(Integer idVencimiento) {
        this.idVencimiento = idVencimiento;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Integer getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(Integer idRegla) {
        this.idRegla = idRegla;
    }

    public Integer getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(Integer puntaje) {
        this.puntaje = puntaje;
    }

    public Integer getPuntajeUtilizado() {
        return puntajeUtilizado;
    }

    public void setPuntajeUtilizado(Integer puntajeUtilizado) {
        this.puntajeUtilizado = puntajeUtilizado;
    }

    public Integer getSaldo() {
        return saldo;
    }

    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
