package py.com.prueba.laboratorio.modelo;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "persona")
public class Persona implements Serializable {

    @Id
    @Column(name = "id_persona")
    @Basic(optional = false)
    @GeneratedValue(generator = "personaSec")
    @SequenceGenerator(name = "personaSec",sequenceName = "persona_sec",allocationSize = 0)
    private Integer idPersona;


    @Column(name = "nombre", length = 50)
    @Basic(optional = false)
    private String nombre;

    @Column(name = "apellido", length = 50)
    @Basic(optional = false)
    private String apellido;

    @Column(name = "nro_documento", length = 50)
    @Basic(optional = false)
    private String nroDocumento;

    @Column(name = "tipo_documento", length = 50)
    @Basic(optional = false)
    private String tipoDocumento;

    @Column(name = "nacionalidad", length = 50)
    @Basic(optional = true)
    private String nacionalidad;

    @Column(name = "email", length = 50)
    @Basic(optional = true)
    private String email;

    @Column(name = "telefono", length = 50)
    @Basic(optional = true)
    private Integer telefono;

    @Column(name = "fecha_nac", length = 50)
    @Basic(optional = true)
    @JsonbDateFormat(value = "dd-MM-yyyy")
    private LocalDate fechaNac;

    //@OneToMany(mappedBy = "persona")
    //private List<Agenda> listaAgendas;

    public Persona(){

    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public LocalDate getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(LocalDate fechaNac) {
        this.fechaNac = fechaNac;
    }

    /*public List<Agenda> getListaAgendas() {
        return listaAgendas;
    }

    public void setListaAgendas(List<Agenda> listaAgendas) {
        this.listaAgendas = listaAgendas;
    }*/

    @Override
    public String toString() {
        return nombre+ " "+apellido;
    }
}
