package py.com.prueba.laboratorio.modelo;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "puntocabecera")
public class PuntoCabecera implements Serializable {

    @Id
    @Column(name = "id_puntocabecera")
    @Basic(optional = false)
    @GeneratedValue(generator = "puntocabeceraSec")
    @SequenceGenerator(name = "puntocabeceraSec",sequenceName = "puntocabecera_sec",allocationSize = 0)
    private Integer idPuntoCabecera;

    @Column(name = "id_persona")
    @Basic(optional = false)
    private Integer idPersona;

    @Column(name = "puntaje_usado")
    @Basic(optional = false)
    private Integer puntaje_usado;

    @Column(name = "id_vale")
    @Basic(optional = false)
    private Integer idVale;

    @Column(name = "concepto_vale")
    @Basic(optional = false)
    private String conceptoVale;

    @Column(name = "fecha_uso", length = 50)
    @Basic(optional = true)
    @JsonbDateFormat(value = "dd-MM-yyyy")
    private LocalDate fecha_uso;


    public PuntoCabecera(){
    }

    public Integer getIdPuntoCabecera() {
        return idPuntoCabecera;
    }

    public Integer getPuntaje_usado() {
        return puntaje_usado;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public LocalDate getFecha_uso() {
        return fecha_uso;
    }

    public void setIdPuntoCabecera(Integer idPuntoCabecera) {
        this.idPuntoCabecera = idPuntoCabecera;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public void setPuntaje_usado(Integer puntaje_usado) {
        this.puntaje_usado = puntaje_usado;
    }

    public void setFecha_uso(LocalDate fecha_uso) {
        this.fecha_uso = fecha_uso;
    }

    public Integer getIdVale() {
        return idVale;
    }

    public void setIdVale(Integer idVale) {
        this.idVale = idVale;
    }

    public String getConceptoVale() {
        return conceptoVale;
    }

    public void setConceptoVale(String conceptoVale) {
        this.conceptoVale = conceptoVale;
    }
}
