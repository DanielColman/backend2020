package py.com.prueba.laboratorio.modelo;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


@Entity
@Table(name = "puntodetalle")

public class PuntoDetalle implements Serializable {

    @Id
    @Column(name = "id_puntodetalle")
    @Basic(optional = false)
    @GeneratedValue(generator = "puntoDetalleSec")
    @SequenceGenerator(name = "puntoDetalleSec",sequenceName = "puntodetalle_sec",allocationSize = 0)
    private Integer idPuntoDetalle;

    @Column(name = "id_puntocabecera")
    @Basic(optional = false)
    private Integer idPuntoCabecera;

    @Column(name = "puntaje_usado")
    @Basic(optional = false)
    private Integer puntaje_usado;

    @Column(name = "id_bolsa")
    @Basic(optional = false)
    private Integer idBolsa;

    public Integer getIdPuntoDetalle() {
        return idPuntoDetalle;
    }

    public void setIdPuntoDetalle(Integer idPuntoDetalle) {
        this.idPuntoDetalle = idPuntoDetalle;
    }

    public Integer getIdPuntoCabecera() {
        return idPuntoCabecera;
    }

    public void setIdPuntoCabecera(Integer idPuntoCabecera) {
        this.idPuntoCabecera = idPuntoCabecera;
    }

    public Integer getPuntaje_usado() {
        return puntaje_usado;
    }

    public void setPuntaje_usado(Integer puntaje_usado) {
        this.puntaje_usado = puntaje_usado;
    }

    public Integer getIdBolsa() {
        return idBolsa;
    }

    public void setIdBolsa(Integer idBolsa) {
        this.idBolsa = idBolsa;
    }
}
