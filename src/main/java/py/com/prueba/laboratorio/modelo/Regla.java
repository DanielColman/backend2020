package py.com.prueba.laboratorio.modelo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;



@Entity
@Table(name = "regla")

public class Regla implements Serializable {
    @Id
    @Column(name = "id_regla")
    @Basic(optional = false)
    @GeneratedValue(generator = "reglaSec")
    @SequenceGenerator(name = "reglaSec",sequenceName = "regla_sec",allocationSize = 0)
    private Integer idRegla;

    @Column(name = "limite_inferior", length = 50)
    @Basic(optional = true)
    private Integer limiteInferior;

    @Column(name = "limite_superior", length = 50)
    @Basic(optional = true)
    private Integer limiteSuperior;

    @Column(name = "monto", length = 50)
    @Basic(optional = false)
    private Integer monto;

    public Integer getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(Integer idRegla) {
        this.idRegla = idRegla;
    }

    public Integer getLimiteInferior() {
        return limiteInferior;
    }

    public void setLimiteInferior(Integer limiteInferior) {
        this.limiteInferior = limiteInferior;
    }

    public Integer getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(Integer limiteSuperior) {
        this.limiteSuperior = limiteSuperior;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }
}
