package py.com.prueba.laboratorio.modelo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "vale")

public class Vale implements Serializable {
    @Id
    @Column(name = "id_vale")
    @Basic(optional = false)
    @GeneratedValue(generator = "valeSec")
    @SequenceGenerator(name = "valeSec",sequenceName = "vale_sec",allocationSize = 0)
    private Integer idVale;

    @Column(name = "nombre", length = 50)
    @Basic(optional = false)
    private String nombre;

    @Column(name = "descripcion", length = 100)
    @Basic(optional = true)
    private String descripcion;

    @Column(name = "puntosReq", length = 100)
    @Basic(optional = false)
    private Integer puntosReq;

    public Integer getIdVale() {
        return idVale;
    }

    public void setIdVale(Integer idVale) {
        this.idVale = idVale;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPuntosReq() {
        return puntosReq;
    }

    public void setPuntosReq(Integer puntosReq) {
        this.puntosReq = puntosReq;
    }
}
