package py.com.prueba.laboratorio.modelo;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.time.LocalDate;
import java.util.List;


@Entity
@Table(name = "vencimiento")
public class Vencimiento implements Serializable {

    @Id
    @Column(name = "id_vencimiento")
    @Basic(optional = false)
    @GeneratedValue(generator = "vencimientoSec")
    @SequenceGenerator(name = "vencimientoSec",sequenceName = "vencimiento_sec",allocationSize = 0)
    private Integer idVencimiento;

    @Column(name = "fecha_inicio", length = 50)
    @Basic(optional = false)
    @JsonbDateFormat(value = "dd-MM-yyyy")
    private LocalDate fechaInicio;

    @Column(name = "fecha_fin", length = 50)
    @Basic(optional = false)
    @JsonbDateFormat(value = "dd-MM-yyyy")
    private LocalDate fechaFin;

    @Column(name = "duracion_dias", length = 50)
    @Basic(optional = false)
    private Integer duracionDias;

    public Integer getIdVencimiento() {
        return idVencimiento;
    }

    public void setIdVencimiento(Integer idVencimiento) {
        this.idVencimiento = idVencimiento;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getDuracionDias() {
        return duracionDias;
    }

    public void setDuracionDias(Integer duracionDias) {
        this.duracionDias = duracionDias;
    }
}
