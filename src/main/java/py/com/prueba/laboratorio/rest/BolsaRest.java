package py.com.prueba.laboratorio.rest;

import py.com.prueba.laboratorio.ejb.BolsaDAO;
import py.com.prueba.laboratorio.modelo.Bolsa;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("bolsa")
@Consumes("application/json")
@Produces("application/json")
public class BolsaRest {

    @Inject
    BolsaDAO bolsaDAO;

    @POST
    @Path("/")
    public Response agregar(Bolsa bolsa) {
        bolsaDAO.agregar(bolsa);
        return Response.ok().build();
    }

    @PUT
    @Path("/")
    public Response modificar(Bolsa bolsa) {
        bolsaDAO.modificar(bolsa);
        return Response.ok().build();
    }

    @DELETE
    @Path("/")
    public Response eliminar(Bolsa bolsa) {
        bolsaDAO.eliminar(bolsa.getIdBolsa());
        return Response.ok().build();
    }

    @GET
    @Path("/")
    public Response lista() {
        return Response.ok(bolsaDAO.lista()).build();
    }

    @GET
    @Path("/{id}")
    public Response lista(@PathParam("id") String id) {
        return Response.ok(bolsaDAO.lista(id)).build();
    }

    @GET
    @Path("/rango/{rango1}-{rango2}")
    public Response listaRango(@PathParam("rango1") Integer rango1, @PathParam("rango2") Integer rango2) {
        return Response.ok(bolsaDAO.listaRango(rango1,rango2)).build();
    }

    @GET
    @Path("/vence/{dias}")
    public Response listaVence(@PathParam("dias") Integer dias) {
        return Response.ok(bolsaDAO.listaVence(dias)).build();
    }

}
