package py.com.prueba.laboratorio.rest;

import py.com.prueba.laboratorio.ejb.PersonaDAO;
import py.com.prueba.laboratorio.modelo.Persona;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.time.LocalDate;

@Path("persona")
@Consumes("application/json")
@Produces("application/json")
public class PersonaRest {

    @Inject
    PersonaDAO personaDAO;

    @POST
    @Path("/")
    public Response agregar(Persona persona) {
        personaDAO.agregar(persona);
        return Response.ok().build();
    }

    @PUT
    @Path("/")
    public Response modificar(Persona persona) {
        personaDAO.modificar(persona);
        return Response.ok().build();
    }

    @DELETE
    @Path("/")
    public Response eliminar(Persona persona) {
        personaDAO.eliminar(persona.getIdPersona());
        return Response.ok().build();
    }

    @GET
    @Path("/")
    public Response lista() {
        return Response.ok(personaDAO.lista()).build();
    }

    @GET
    @Path("/{cedula}")
    public Response lista(@PathParam("cedula") String id) {
        return Response.ok(personaDAO.lista(id)).build();
    }

    @GET
    @Path("/nombre/{nombre}")
    public Response listaNombre(@PathParam("nombre") String id) {
        return Response.ok(personaDAO.listaNombre(id)).build();
    }

    @GET
    @Path("/apellido/{apellido}")
    public Response listaApellido(@PathParam("apellido") String id) {
        return Response.ok(personaDAO.listaApellido(id)).build();
    }

    @GET
    @Path("/cumple/{cumple}")
    public Response listaCumple(@PathParam("cumple") String id) {
        return Response.ok(personaDAO.listaCumple(id)).build();
    }



}
