package py.com.prueba.laboratorio.rest;

import py.com.prueba.laboratorio.ejb.PuntoCabeceraDAO;
import py.com.prueba.laboratorio.modelo.PuntoCabecera;


import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("cabecera")
@Consumes("application/json")
@Produces("application/json")
public class PuntoCabeceraRest {

    @Inject
    PuntoCabeceraDAO puntoCabeceraDAO;

    @POST
    @Path("/")
    public Response agregar(PuntoCabecera punto_cabecera) {
        puntoCabeceraDAO.agregar(punto_cabecera);
        return Response.ok().build();
    }

    @PUT
    @Path("/")
    public Response modificar(PuntoCabecera punto_cabecera) {
        puntoCabeceraDAO.modificar(punto_cabecera);
        return Response.ok().build();
    }

    @DELETE
    @Path("/")
    public Response eliminar(PuntoCabecera punto_cabecera) {
        puntoCabeceraDAO.eliminar(punto_cabecera.getIdPuntoCabecera());
        return Response.ok().build();
    }

    @GET
    @Path("/")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response lista() {
        return Response.ok(puntoCabeceraDAO.lista()).build();
    }


    @GET
    @Path("/cliente/{cedula}")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response usoCliente(@PathParam("cedula") String cedula) {
        return Response.ok(puntoCabeceraDAO.listaCliente(cedula)).build();
    }

    @GET
    @Path("/fecha/{fecha}")
    public Response listaFecha(@PathParam("fecha") String id) {
        return Response.ok(puntoCabeceraDAO.listaFecha(id)).build();
    }


    @GET
    @Path("/uso/{id}")
    public Response listaUso(@PathParam("id") Integer id) {
        return Response.ok(puntoCabeceraDAO.listaUso(id)).build();
    }

}