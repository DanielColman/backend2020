package py.com.prueba.laboratorio.rest;

import py.com.prueba.laboratorio.ejb.PuntoCabeceraDAO;
import py.com.prueba.laboratorio.ejb.PuntoDetalleDAO;
import py.com.prueba.laboratorio.modelo.PuntoCabecera;
import py.com.prueba.laboratorio.modelo.PuntoDetalle;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("detalle")
@Consumes("application/json")
@Produces("application/json")
public class PuntoDetalleRest {

    @Inject
    PuntoDetalleDAO puntoDetalleDAO;

    @POST
    @Path("/")
    public Response agregar(PuntoDetalle punto_detalle) {
        puntoDetalleDAO.agregar(punto_detalle);
        return Response.ok().build();
    }

    @PUT
    @Path("/")
    public Response modificar(PuntoDetalle punto_detalle) {
        puntoDetalleDAO.modificar(punto_detalle);
        return Response.ok().build();
    }

    @DELETE
    @Path("/")
    public Response eliminar(PuntoDetalle punto_detalle) {
        puntoDetalleDAO.eliminar(punto_detalle.getIdPuntoDetalle());
        return Response.ok().build();
    }

    @GET
    @Path("/")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response lista() {
        return Response.ok(puntoDetalleDAO.lista()).build();
    }

}