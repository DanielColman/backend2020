package py.com.prueba.laboratorio.rest;

import py.com.prueba.laboratorio.ejb.PersonaDAO;
import py.com.prueba.laboratorio.ejb.ReglaDAO;
import py.com.prueba.laboratorio.modelo.Persona;
import py.com.prueba.laboratorio.modelo.Regla;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("regla")
@Consumes("application/json")
@Produces("application/json")

public class ReglaRest {
    @Inject
    ReglaDAO reglaDAO;

    @POST
    @Path("/")
    public Response agregar(Regla regla) {
        reglaDAO.agregar(regla);
        return Response.ok().build();
    }

    @PUT
    @Path("/")
    public Response modificar(Regla regla) {
        reglaDAO.modificar(regla);
        return Response.ok().build();
    }

    @DELETE
    @Path("/")
    public Response eliminar(Regla regla) {
        reglaDAO.eliminar(regla.getIdRegla());
        return Response.ok().build();
    }

    @GET
    @Path("/")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response lista() {
        return Response.ok(reglaDAO.lista()).build();
    }


    @GET
    @Path("/cuantos/{idRegla}/{monto}")
    public Response cuantos(@PathParam("idRegla") Integer idRegla, @PathParam("monto") Integer monto) {
        return Response.ok(reglaDAO.cuantos(idRegla,monto)).build();
    }
}
