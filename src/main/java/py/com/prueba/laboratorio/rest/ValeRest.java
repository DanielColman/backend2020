package py.com.prueba.laboratorio.rest;

import py.com.prueba.laboratorio.ejb.ValeDAO;
import py.com.prueba.laboratorio.modelo.Vale;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("vale")
@Consumes("application/json")
@Produces("application/json")

public class ValeRest {
    @Inject
    ValeDAO valeDAO;

    @POST
    @Path("/")
    public Response agregar(Vale vale) {
        valeDAO.agregar(vale);
        return Response.ok().build();
    }

    @PUT
    @Path("/")
    public Response modificar(Vale vale) {
        valeDAO.modificar(vale);
        return Response.ok().build();
    }

    @DELETE
    @Path("/")
    public Response eliminar(Vale vale) {
        valeDAO.eliminar(vale.getIdVale());
        return Response.ok().build();
    }


    @GET
    @Path("/")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response lista() {
        return Response.ok(valeDAO.lista()).build();
    }
}
