package py.com.prueba.laboratorio.rest;


import py.com.prueba.laboratorio.ejb.VencimientoDAO;
import py.com.prueba.laboratorio.modelo.Vencimiento;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Path("vencimiento")
@Consumes("application/json")
@Produces("application/json")
public class VencimientoRest {

    @Inject
    VencimientoDAO vencimientoDAO;

    @POST
    @Path("/")
    public Response agregar(Vencimiento vencimiento) {
        vencimientoDAO.agregar(vencimiento);
        return Response.ok().build();
    }

    @PUT
    @Path("/")
    public Response modificar(Vencimiento vencimiento) {
        vencimientoDAO.modificar(vencimiento);
        return Response.ok().build();
    }

    @DELETE
    @Path("/")
    public Response eliminar(Vencimiento vencimiento) {
        vencimientoDAO.eliminar(vencimiento.getIdVencimiento());
        return Response.ok().build();
    }

    @GET
    @Path("/")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response lista() {
        return Response.ok(vencimientoDAO.lista()).build();
    }

}
